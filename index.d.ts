import expressSession, {
	MemoryStore as ExpressMemoryStore,
} from "express-session";

interface SailsSessionStoreOptions {
    /**
     * The name of the Sails model to use for storing sessions.
     * The Sails app must be globally accessible, and this model
     * must exist as a property of sails.models.
     * 
     * TODO: Find a way to nix this requirement so models can be
     * defined without them or sails being global.
     */
    model: string;
	/**
     * Define the interval in a number of milliseconds that prune checks
     * should run (deleting expired session records). Defaults to one
     * hour. Ideally, do not check any more frequently than every
     * 15 minutes to avoid strain on your ORM / database.
	 */
	checkPeriod?: number;
	/**
	 * Session TTL (expiration) in milliseconds.
	 * Defaults to `session.maxAge` (if set), or one day.
	 */
	ttl?: number | ((options: any, sess: any, sessionId: any) => number);
	/**
	 * If true, then any session ID requested that does not exist in
	 * the database will be created with a blank data session / cookie.
	 * If false (the default), getting a session that does not exist
	 * will callback with null for the session.
	 */
	createIfNotExists?: boolean;
	/**
	 * The maximum size of the cache, checked by applying the length
	 * function to all values in the cache (used to help avoid repeat
	 * queries to the sails model). It defaults to 1000.
	 */
	max?: number;
}

declare class MemoryStore extends ExpressMemoryStore {
	constructor(options: SailsSessionStoreOptions);
	/** method to start the timer for pruning expired sessions. */
	startInterval(): void;
	/** method to clear the timer for pruning expired sessions. */
	stopInterval(): void;
	/** use to manually remove expired sessions. */
	prune(): void;
}

/**
 * Sample usage:
 * ```
 * import session from 'express-session';
 * import createMemoryStore from 'memorystore';
 * const MemoryStore = createMemoryStore(session);
 * ...
 * app.use(session({ store: new MemoryStore({ ...options }) }));
 * ```
 */
declare function createMemoryStore(
	session: typeof expressSession
): typeof MemoryStore;
export = createMemoryStore;