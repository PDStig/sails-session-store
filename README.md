# sails-session-store 0.2.0 alpha

This is an express-session store for use with SailsJS(https://sailsjs.com). Specifically, this store allows you to build your own Waterline ORM model in your Sails app and use that for storing sessions.

## Development

Caution! This store is in alpha stage. Bugs are to be expected. Please report bugs or suggestions as issues. I do not recommend using this in production yet.

## Setup

First, in your Sails project repository, run this command:

    $ npm install https://gitlab.com/PDStig/sails-session-store.git

Then, you need to create a model in your Sails app for storing your sessions. It must contain the following attributes:

```javascript
module.exports = {
  attributes: {
    // I am assuming you have a primary key id configured in your config/models.js file.

    sessionID: {
      type: "string",
      required: true,
      unique: true,
    },

    data: {
      type: "json",
    },

    ttl: {
      type: "number",
      defaultsTo: 24 * 60 * 60 * 1000, // The store will use cookie.maxAge when possible. This is just a schema fallback.
    },
  },
}
```

Next, you need to specify your session settings in config/session.js. Here is an example:

```javascript
module.exports.session = {
  adapter: "@pdstig/sails-session-store",
  name: "sails.sid",
  model: "YOUR_ORM_MODEL_NAME_HERE",
  secret: "DO NOT FORGET A SECRET!",
};
```

Do not forget to specify a secret. And ideally, you should specify cookie settings for env/production.js especially secure: true.

Finally, **you must expose sails globally** or the store will not work! Hopefully in the future, this requirement can be remedied as this is not ideal.

## Contributing

At this time, this is just a low-effort store I made for a particular application need. Contributors are welcome to submit pull requests and issues to help improve this store to be production-ready for others to use. Thank you for your generosity to the open source community!

Namely, the following could use your help:
 - CI/CD for publishing to NPM
 - More elegant solution for using sails / models instances in the SailsSessionStore without requiring globals
 - mocha test suites