/*!
 * sails-session-store
 * Copyright(c) 2023 PDStig, LLC
 * MIT Licensed
 * Based off of memorystore by Rocco Musolino
 */

const { LRUCache } = require("lru-cache");

/**
 * One day in milliseconds.
 */

const oneDay = 86400000;

function getTTL(options, sess, sid) {
  // An explicit expiration was defined in the cookie. Calculate the ttl from that instead.
  if (sess && sess.cookie && sess.cookie._expires) {
    let expiration = new Date(sess.cookie._expires);
    let expirationMilliseconds = expiration.getTime();
    let nowTime = new Date();
    let nowTimeMilliseconds = nowTime.getTime();
    return Math.floor(expirationMilliseconds - nowTimeMilliseconds);
  }

  if (typeof options.ttl === "number") return options.ttl;
  if (typeof options.ttl === "function") return options.ttl(options, sess, sid);
  if (options.ttl)
    throw new TypeError("`options.ttl` must be a number or function.");

  let maxAge = sess && sess.cookie ? sess.cookie.maxAge : null;
  return typeof maxAge === "number" ? Math.floor(maxAge) : oneDay;
}

function sailsModel(store) {
  if (typeof store.sails !== "object") {
    throw new Error("Sails instance not found.");
  }

  if (typeof store.model !== "string") {
    throw new TypeError(
      "You must define the name of a sails ORM model to use for storing sessions. It must have the attributes sessionID (string / unique), data (JSON), ttl (number), createdAt, and updatedAt."
    );
  }

  if (typeof store.sails.models[store.model] !== "object") {
    console.log(store.model);
    throw new Error("Could not find the provided sails model.");
  }

  return store.sails.models[store.model];
}

let defer =
  typeof setImmediate === "function"
    ? setImmediate
    : function (fn) {
        process.nextTick(fn.bind.apply(fn, arguments));
      };

/**
 * Return the `SailsSessionStore` extending `express`'s session Store.
 *
 * @param {object} session express-session
 * @return {Function}
 * @api public
 */

module.exports = function (session) {
  /**
   * Express's session Store.
   */

  let Store = session.Store;

  /**
   * Initialize SailsSessionStore with the given `options`.
   *
   * @param {Object} options
   * @api public
   */

  function SailsSessionStore(options) {
    if (!(this instanceof SailsSessionStore)) {
      throw new TypeError(
        "Cannot call SailsSessionStore constructor as a function"
      );
    }

    options = options || {};
    Store.call(this, options);

    this.options = {};
    this.options.checkPeriod = options.checkPeriod || 1000 * 60 * 60; // Every hour by default
    this.options.ttl = options.ttl;
    this.options.createIfNotExists = options.createIfNotExists || false;
    this.options.max = options.max || 1000;
    this.serializer = JSON;

    // Sails instance (TODO: Find a way to pass a sails instance instead of requiring it to be global)
    this.sails = sails;
    if (typeof this.sails !== "object") {
      throw new Error(
        "For the time being, you must expose sails globally for the session store to work."
      );
    }

    // Sails model
    this.model = options.model;
    if (typeof this.model !== "string") {
      throw new TypeError(
        "You must define the name of a sails ORM model to use for storing sessions. It must have the attributes sessionID (string / unique), data (JSON), ttl (number), createdAt, and updatedAt."
      );
    }

    // Create our cache
    this.cache = new LRUCache({
      ttl: 1000 * 60 * 60, // We only want the default ttl for the cache to be one hour; actual session ttl will be stored in the sails model.
      max: this.options.max,
      allowStale: false, // Should never allow this for this particular store because we want to trigger a sails model query if stale.
    });

    this.startInterval();
  }

  /**
   * Inherit from `Store`.
   */

  SailsSessionStore.prototype.__proto__ = Store.prototype;

  /**
   * Attempt to fetch session by the given `sid`.
   *
   * @param {String} sid
   * @param {Function} fn
   * @api public
   */
  SailsSessionStore.prototype.get = function (sid, fn) {
    // Attempt to get the session from cache first
    this.getCache(sid, (err, cSession) => {
      // Cache hit
      if (cSession) {
        fn && defer(fn, err, cSession);
        return;
      }

      // Cache miss; run a sails model query.

      let store = sailsModel(this);

      // Create the session if it does not exist if createIfNotExists is true.
      if (this.options.createIfNotExists) {
        store
          .findOrCreate(
            { sessionID: sid },
            {
              sessionID: sid,
              data: {
                cookie: {
                  httpOnly: true,
                  path: "/",
                },
              },
              ttl: getTTL(this.options, {}, sid),
            }
          )
          .exec((err, session, wasCreated) => {
            if (wasCreated) this.setCache(sid, session.data);
            fn && defer(fn, err, session ? session.data : null);
          });
      } else {
        store.findOne({ sessionID: sid }).exec((err, session) => {
          if (session && session.data) this.setCache(sid, session.data);

          fn && defer(fn, err, session ? session.data : null);
        });
      }
    });
  };

  /**
   * Attempt to fetch session by the given `sid` but only from cache.
   *
   * @param {String} sid
   * @param {Function} fn
   * @api public
   */
  SailsSessionStore.prototype.getCache = function (sid, fn) {
    let data = this.cache.get(sid);
    if (!data) return fn();

    let err = null;
    let result;
    try {
      result = this.serializer.parse(data);
    } catch (er) {
      err = er;
    }
    fn && defer(fn, err, result);
  };

  /**
   * Save the `sess` object associated with the given `sid`.
   *
   * @param {String} sid
   * @param {Session} sess
   * @param {Function} fn
   * @api public
   */

  SailsSessionStore.prototype.set = function (sid, sess, fn) {
    // "Async" store session into cache. We don't care if it errors; the sails model is what we care about.
    this.setCache(sid, sess);

    let data = Object.assign({}, sess);
    let ttl = getTTL(this.options, sess, sid);
    let store = sailsModel(this);
    store
      .findOrCreate(
        { sessionID: sid },
        { sessionID: sid, data: data, ttl: ttl }
      )
      .exec((err, session, wasCreated) => {
        if (wasCreated) {
          fn && defer(fn, err);
        } else {
          store
            .updateOne({ sessionID: sid }, { data: data, ttl: ttl })
            .exec((err, session) => {
              fn && defer(fn, err);
            });
        }
      });
  };

  /**
   * Save the `sess` object associated with the given `sid` but only in the cache.
   *
   * @param {String} sid
   * @param {Session} sess
   * @param {Function} fn
   * @api public
   */

  SailsSessionStore.prototype.setCache = function (sid, sess, fn) {
    let ttl = getTTL(this.options, sess, sid);
    try {
      var jsess = this.serializer.stringify(sess);
    } catch (err) {
      fn && defer(fn, err);
    }

    this.cache.set(sid, jsess, Math.min(ttl, 1000 * 60 * 60)); // ttl for cache should be no more than one hour.
    fn && defer(fn, null);
  };

  /**
   * Destroy the session associated with the given `sid`.
   *
   * @param {String} sid
   * @api public
   */

  SailsSessionStore.prototype.destroy = function (sid, fn) {
    // Delete from cache
    if (Array.isArray(sid)) {
      sid.forEach(function (s) {
        this.cache.delete(s);
      });
    } else {
      this.cache.delete(sid);
    }

    // Delete from sails model
    let store = sailsModel(this);
    store.destroy({ sessionID: sid }).exec((err, sessions) => {
      fn && defer(fn, err);
    });
  };

  /**
   * Refresh the time-to-live for the session with the given `sid`.
   *
   * @param {String} sid
   * @param {Session} sess
   * @param {Function} fn
   * @api public
   */

  SailsSessionStore.prototype.touch = function (sid, sess, fn) {
    let store = sailsModel(this);
    let ttl = getTTL(this.options, sess, sid);

    store.updateOne({ sessionID: sid }, { ttl: ttl }).exec((err, session) => {
      this.setCache(sid, session.data);
      fn && defer(fn, err);
    });
  };

  /**
   * Fetch all sessions' ids
   *
   * @param {Function} fn
   * @api public
   */

  SailsSessionStore.prototype.ids = function (fn) {
    let store = sailsModel(this);
    let IDs = [];

    // Use the sails model for this, not the cache, because we have a shorter ttl on the cache.
    store
      .stream()
      .eachRecord((record, next) => {
        IDs.push(record.sessionID);
        next();
      })
      .exec(function (err) {
        fn && defer(fn, err, IDs);
      });
  };

  /**
   * Fetch all sessions
   *
   * @param {Function} fn
   * @api public
   */

  SailsSessionStore.prototype.all = function (fn) {
    let store = this.store;
    let sessions = {};

    // Use the sails model for this, not the cache, because we have a shorter ttl on the cache.
    store
      .stream()
      .eachRecord((record, next) => {
        sessions[record.sessionID] = record.data;
        next();
      })
      .exec(function (err) {
        fn && defer(fn, err, sessions);
      });
  };

  /**
   * Delete all sessions from the store
   *
   * @param {Function} fn
   * @api public
   */

  SailsSessionStore.prototype.clear = function (fn) {
    // Remove everything from the cache
    this.cache.clear();

    // Remove everything from the sails model
    let store = sailsModel(this);
    store.destroy().exec((err) => {
      fn && defer(fn, err);
    });
  };

  /**
   * Get the count of all sessions in the store
   *
   * @param {Function} fn
   * @api public
   */

  SailsSessionStore.prototype.length = function (fn) {
    let store = sailsModel(this);

    // Use the sails model for this, not the cache, because we have a shorter ttl on the cache.
    store.count().exec((err, count) => {
      fn && defer(fn, err, count);
    });
  };

  /**
   * Start the check interval
   * @api public
   */

  SailsSessionStore.prototype.startInterval = function () {
    let self = this;
    let ms = this.options.checkPeriod;
    if (ms && typeof ms === "number") {
      clearInterval(this._checkInterval);
      this._checkInterval = setInterval(function () {
        self.prune();
      }, Math.floor(ms)).unref();
    }
  };

  /**
   * Stop the check interval
   * @api public
   */

  SailsSessionStore.prototype.stopInterval = function () {
    clearInterval(this._checkInterval);
  };

  /**
   * Remove only expired entries from the store
   * @api public
   */

  SailsSessionStore.prototype.prune = function () {
    // Run a general purge on the cache, though we will still delete specific records individually if found expired.
    this.cache.purgeStale();

    let store = sailsModel(this);

    // Use batch processing as there could be a lot of records
    store
      .stream()
      .eachRecord((record, next) => {
        let updatedAt = new Date(record.updatedAt);
        let updatedAtMilliseconds = updatedAt.getTime();
        let expiration = updatedAtMilliseconds + record.ttl;
        let nowTime = new Date();
        let nowTimeMilliseconds = nowTime.getTime();

        // Is it expired?
        if (expiration < nowTimeMilliseconds) {
          // Delete from cache
          this.cache.delete(record.sessionID);

          // Delete from the sails model
          store.destroy({ sessionID: record.sessionID }).exec((err) => {
            if (err) throw err;

            next();
          });
        } else {
          next();
        }
      })
      .exec(function (err) {
        if (err) throw err;
      });
  };

  return SailsSessionStore;
};
